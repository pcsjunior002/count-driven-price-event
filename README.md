/**************************************************************************************************

To install the Count Driven Price Event utility:

Please forgive me the over-explained instructions.

You will need:

1.  Tessitura version 12.5.1 or later (this procedure utilizes the pricing event functionality)
2.  SSMS access/Database Administrator
3.  Tessitura Security Access
4.  Two dedicated id numbers in TR_PRICING_RULE_CATEGORY
5.  Use of AP_SEND_SMTP_MAIL (sp_send_dbmail must be enabled)
6.  The id of your default ticket price layer for pricing updates from TR_PRICE_LAYER_TYPE

        --For disabling price events, this procedure disables all layers.
        --For price change price events, this procedure updates only one default pricing layer.
        --If your organization utilizes more than one layer regularly for ticket prices and
        --pricing events that would need to be updated, I would be more than happy to work with
        --you to update this local table and procedure to fit your needs.  It would require a bit
        --of work, but I think we could get there without too many headaches.  Please feel free
        --to send me an e-mail at jmoskal@TheCenterPresents.org.

7.  The contents of this archive (LP_CP_COUNT_DRIVEN_PRICE_EVENT and LTR_COUNT_DRIVEN_PRICE_EVENT)

Installation Steps:

1.  Create LTR_COUNT_DRIVEN_PRICE_EVENT
2.  Grant database rights (SELECT, INSERT, UPDATE and DELETE) for local table to ImpUsers
3.  Run UP_POPULATE_REFERENCE_METADATA on local table
4.  Insert into TR_PRICING_RULE_CATEGORY two rows (below).  Keep the two id numbers handy.

    Disable Price Zone
    
	Pricing Change

        --I would recommend putting a 'z' in front of these so that they are at the bottom
		--of the list in the actual pricing rules module in Tessitura since they will not
		--be used for any of those rules.  I decided to still put them in this table however
		--as both a reminder to staff that these options will be available as well as the
		--fact that this tends to fit with their designated function and operation.
        
5.  Insert any desired help text into TR_REFERENCE_TABLE.  Sample help text provided in another
    document.  This other document also has further explanations as to how the procedure functions.
6.  Update the 'action' column in TR_REFERENCE_COLUMN.

	SET Dddw Table = TR_PRICING_RULE_CATEGORY

    SET Dddw Value = id
    
	SET Dddw Description = description
    
	SET Dddw Where = id IN ([YOUR TWO ID VALUES FROM #4]), E.g. Dddw Where = id IN (3,4)
    
	Also verify that the row for the inactive column has the Checkbox column checked.
    
7.  Grant access to local table to relevant user groups in Tessitura Security
8.  Run the Create LP_CP_COUNT_DRIVEN_PRICE_EVENT script, being sure to make the necessary updates
    to the procedure before running.
9.  Schedule the procedure to run every few minutes, using the e-mail list of all staff members you
    desire to notify when the procedure is enacted as the parameter.  This has been tested with a
    schedule of every three minutes, but I have no reason to believe it could not be scheduled for
    a shorter period if desired.
10. Begin inserting rows into LTR_COUNT_DRIVEN_PRICE_EVENT based on your sales needs.

That's it!

For any questions or issues, please do not hesitate to contact me:

    John A. Moskal II
    jmoskal@TheCenterPresents.org
***************************************************************************************************/